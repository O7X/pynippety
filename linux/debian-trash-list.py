from getpass import getuser
from os import listdir
from os.path import isdir, isfile, join
username = getuser()
trash_dir_location = f'/home/{username}/.local/share/Trash/files'
trash_directories = sorted([f for f in listdir(trash_dir_location) if isdir(join(trash_dir_location, f))])
trash_files = sorted([f for f in listdir(trash_dir_location) if isfile(join(trash_dir_location, f))])
trash_directories_list = []
i = -1
for d in sorted(trash_directories):
    i += 1
    trash_directories_list.append((i, "[dir]" + d))

trash_files_list = []
x = i
for f in sorted(trash_files):
    x += 1
    trash_files_list.append((x, "[file]" + f))

all_files = trash_directories_list + trash_files_list

for n in all_files:
    print(n)

